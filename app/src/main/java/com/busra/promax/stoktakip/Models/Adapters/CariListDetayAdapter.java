package com.busra.promax.stoktakip.Models.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.busra.promax.stoktakip.Models.CariModel;
import com.busra.promax.stoktakip.R;

import java.util.ArrayList;

public class CariListDetayAdapter extends ArrayAdapter<CariModel> {

    private final LayoutInflater inflater;
    private final Context context;
    private ViewHolder holder;
    private final ArrayList<CariModel> cariModels;

    public CariListDetayAdapter(Context context, ArrayList<CariModel> cariModels) {
        super(context,0, cariModels);
        this.context = context;
        this.cariModels = cariModels;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return cariModels.size();
    }

    @Override
    public CariModel getItem(int position) {
        return cariModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return cariModels.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.cari_list_detay_item, null);

            holder = new ViewHolder();
            holder.cariId = (TextView) convertView.findViewById(R.id.cari_id_txt);
            holder.cariUnv = (TextView) convertView.findViewById(R.id.cari_unvani_txt);
            holder.cariYet = (TextView) convertView.findViewById(R.id.cari_yet_txt);
            holder.cariGrv = (TextView) convertView.findViewById(R.id.cari_grv_txt);
            holder.cariAdr = (TextView) convertView.findViewById(R.id.cari_adrs_txt);
            holder.cariTel = (TextView) convertView.findViewById(R.id.cari_tel_txt);


            convertView.setTag(holder);

        }
        else{
            //Get viewholder we already created
            holder = (ViewHolder)convertView.getTag();
        }

       /* CariModel cariModel = cariModel.get(position);
        if(person != null){
            holder.personImage.setImageResource(person.getPhotoId());
            holder.personNameLabel.setText(person.getName());
            holder.personAddressLabel.setText(person.getAddress());

        }*/
        return convertView;
    }

    //View Holder Pattern for better performance
    private static class ViewHolder {
        TextView cariId;
        TextView cariUnv;
        TextView cariYet;
        TextView cariGrv;
        TextView cariAdr;
        TextView cariTel;
    }
}