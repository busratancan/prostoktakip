package com.busra.promax.stoktakip;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    ImageView cari,stok,rapor,hesap,settings,cikis;
    SharedPreferences preferences; //preferences nesne referansı
    SharedPreferences.Editor editor; //preferences editor nesnesi referansı .prefernces nesnesine veri ekleyip cıkarmak için

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cari = (ImageView)findViewById(R.id.cari_btn);
        stok = (ImageView)findViewById(R.id.stok_btn);
        rapor = (ImageView)findViewById(R.id.rapor_btn);
        hesap = (ImageView)findViewById(R.id.hesap_btn);
        settings = (ImageView)findViewById(R.id.settings_btn);
        cikis = (ImageView)findViewById(R.id.cikis_btn);

        cari.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                Intent cariGec  = new Intent(MainActivity.this,CariActivity.class);
                startActivity(cariGec);
            }});
        stok.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                Intent stokGec  = new Intent(MainActivity.this,StokActivity.class);
                startActivity(stokGec);
            }});
        rapor.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                Intent raporGec  = new Intent(MainActivity.this,StokActivity.class);
                startActivity(raporGec);
            }});
        hesap.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                Intent hesapGec  = new Intent(MainActivity.this,ProfilActivitiy.class);
                startActivity(hesapGec);
            }});

        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());//preferences nesnesi oluşturuluyor ve prefernces referansına bağlanıyor
        editor = preferences.edit(); //aynı şekil editor nesnesi oluşturuluyor

        cikis.setOnClickListener(new View.OnClickListener() {//SharedPreferences değerlerini  tamamen siler

            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
                alertDialog.setTitle("Bilgi");
                alertDialog.setMessage("Kayıt Bilgileriniz Tamamen Silinecek.Tekrar Kayıt Olmanız Gerekecek ");
                alertDialog.setPositiveButton("Tamam", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int which) {

                        //Burda değerlerin hepsi silindiği için tekrardan uygulamaya giriş yapabilmesi için kayıt olmalıdır
                        editor.clear();//sharedpreferences değerlerinn hepsini siler
                        editor.apply();

                        Intent i = new Intent(getApplicationContext(),ProfilActivitiy.class);
                        startActivity(i);
                        finish();
                    }
                });
                alertDialog.show();
            }
        });
    }
}
