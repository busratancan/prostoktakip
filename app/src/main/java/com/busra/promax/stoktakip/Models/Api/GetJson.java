package com.busra.promax.stoktakip.Models.Api;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.busra.promax.stoktakip.Models.Adapters.CariListAdapter;
import com.busra.promax.stoktakip.Models.CariModel;
import com.busra.promax.stoktakip.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class GetJson extends AppCompatActivity {

    Context context;
    CariListAdapter cariListAdapter;
    ListView recyclerView;
    String url = "https://www.androidevreni.com/dersler/json_veri.txt";

    public GetJson(Context context) {
        this.context = context;
    }

    @Nullable
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getCari();
    }
    public void getCari(){
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            final ArrayList<CariModel> deneme = new ArrayList<>();
            final JsonArrayRequest myReq = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    Log.d("URL", "URL: " + url);
                    Gson gson = new Gson();
                    Type collectionType = new TypeToken<ArrayList<CariModel>>() {
                    }.getType();
                    ArrayList<CariModel> enums = gson.fromJson(response.toString(), collectionType);
                    for (CariModel list : enums) {
                        Log.d("URL", "URL: " + list.getName());
                        deneme.add(list);
                    }
                    cariListAdapter = new CariListAdapter(context, deneme);
                    recyclerView.setAdapter(cariListAdapter);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("TEST", "onErrorResponse: " + error);

                }
            });
            requestQueue.add(myReq);
        }
}
