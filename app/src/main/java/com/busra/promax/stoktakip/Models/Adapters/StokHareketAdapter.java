package com.busra.promax.stoktakip.Models.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.busra.promax.stoktakip.Models.CariModel;
import com.busra.promax.stoktakip.R;

import java.util.ArrayList;

public class StokHareketAdapter extends ArrayAdapter<CariModel> {

    private final LayoutInflater inflater;
    private final Context context;
    private StokHareketAdapter.ViewHolder holder;
    private final ArrayList<CariModel> cariModels;

    public StokHareketAdapter(Context context, ArrayList<CariModel> cariModels) {
        super(context,0, cariModels);
        this.context = context;
        this.cariModels = cariModels;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return cariModels.size();
    }

    @Override
    public CariModel getItem(int position) {
        return cariModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return cariModels.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.stok_hareket_item, null);

            holder = new StokHareketAdapter.ViewHolder();
            holder.cariId = (TextView) convertView.findViewById(R.id.cari_id_txt);
            holder.cariKodu = (TextView) convertView.findViewById(R.id.cari_kodu_txt);
            holder.cariUnv = (TextView) convertView.findViewById(R.id.cari_unvani_txt);
            holder.tarih = (TextView) convertView.findViewById(R.id.tarih_txt);
            holder.islem = (TextView) convertView.findViewById(R.id.islem_txt);
            holder.kar = (TextView) convertView.findViewById(R.id.kar_txt);
            holder.satis = (TextView) convertView.findViewById(R.id.satis_tutari_txt);


            convertView.setTag(holder);

        }
        else{
            //Get viewholder we already created
            holder = (StokHareketAdapter.ViewHolder)convertView.getTag();
        }

       /* CariModel cariModel = cariModel.get(position);
        if(person != null){
            holder.personImage.setImageResource(person.getPhotoId());
            holder.personNameLabel.setText(person.getName());
            holder.personAddressLabel.setText(person.getAddress());

        }*/
        return convertView;
    }

    //View Holder Pattern for better performance
    private static class ViewHolder {
        TextView cariId;
        TextView cariKodu;
        TextView cariUnv;
        TextView tarih;
        TextView islem;
        TextView kar;
        TextView satis;
    }
}