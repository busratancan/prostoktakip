package com.busra.promax.stoktakip;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class SettingsActivity extends Activity {

    Button kayit;
    EditText hst,prt;
    String host,port;
    SharedPreferences preferences; //preferences nesne referansı
    SharedPreferences.Editor editor; //preferences editor nesnesi referansı .prefernces nesnesine veri ekleyip cıkarmak için
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activitiy_settings);

        kayit = (Button) findViewById(R.id.save_btn);
        hst = (EditText)findViewById(R.id.hst_edt);
        prt = (EditText)findViewById(R.id.prt_edt);



        kayit.setOnClickListener(new View.OnClickListener() {//Kayıt ol butonu tıklanınca

            @Override
            public void onClick(View v) {
                host = hst.getText().toString();
                port = prt.getText().toString();

                if(host.matches("") || port.matches("") ){//Veriler Boş  ise

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(SettingsActivity.this);
                    alertDialog.setTitle("Uyarı");
                    alertDialog.setMessage("Eksiksiz Doldurunz!");
                    alertDialog.setPositiveButton("Tamam", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int which) {

                        }
                    });
                    alertDialog.show();
                }else{//veriler boş değilse kayıt işlemine geçilir

                    preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());//preferences nesnesi oluşturuluyor ve prefernces referansına bağlanıyor
                    editor = preferences.edit(); //aynı şekil editor nesnesi oluşturuluyor


                    editor.putString("host", host); //host değeri
                    editor.putString("port", port);//port değeri
                    editor.putBoolean("settings", true);//uygulamaya tekrar girdiğinde kontrol için kullanılcak
                    //editor.putInt("sayısalDeger", 1000);// uygulamamızda kullanılmıyor ama göstermek amacıyla

                    editor.apply();//yapılan değişiklikler kaydedilmesi için editor nesnesinin commit() metodu çağırılır.
                    //Değerlerimizi sharedPreferences a kaydettik.Artık bu bilgiler ile giriş yapabiliriz.
                    Intent i = new Intent(getApplicationContext(),ProfilActivitiy.class);
                    startActivity(i);
                    finish();
                }

            }
        });
    }
}

        /*AppCompatActivity {
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    EditText hst,prt;
    Button save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activitiy_settings);
        hst = (EditText) findViewById(R.id.hst_edt);
        prt = (EditText) findViewById(R.id.prt_edt);
        save = (Button) findViewById(R.id.save_btn);



    }

}*/