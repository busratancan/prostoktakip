package com.busra.promax.stoktakip.Models.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.busra.promax.stoktakip.Models.CariModel;
import com.busra.promax.stoktakip.R;

import java.util.ArrayList;

public class CariHareketAdapter extends ArrayAdapter<CariModel> {

    private final LayoutInflater inflater;
    private final Context context;
    private CariHareketAdapter.ViewHolder holder;
    private final ArrayList<CariModel> cariModels;

    public CariHareketAdapter(Context context, ArrayList<CariModel> cariModels) {
        super(context,0, cariModels);
        this.context = context;
        this.cariModels = cariModels;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return cariModels.size();
    }

    @Override
    public CariModel getItem(int position) {
        return cariModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return cariModels.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.cari_hareket_item, null);

            holder = new CariHareketAdapter.ViewHolder();
            holder.tarih = (TextView) convertView.findViewById(R.id.tarih_txt);
            holder.islem = (TextView) convertView.findViewById(R.id.islem_txt);
            holder.cariBak = (TextView) convertView.findViewById(R.id.cari_bakiye_txt);
            holder.cariBrc = (TextView) convertView.findViewById(R.id.cari_borc_txt);
            holder.cariAlc = (TextView) convertView.findViewById(R.id.cari_alacak_txt);
            holder.cariDes = (TextView) convertView.findViewById(R.id.description_txt);


            convertView.setTag(holder);

        }
        else{
            //Get viewholder we already created
            holder = (CariHareketAdapter.ViewHolder)convertView.getTag();
        }

       /* CariModel cariModel = cariModel.get(position);
        if(person != null){
            holder.personImage.setImageResource(person.getPhotoId());
            holder.personNameLabel.setText(person.getName());
            holder.personAddressLabel.setText(person.getAddress());

        }*/
        return convertView;
    }

    //View Holder Pattern for better performance
    private static class ViewHolder {
        TextView tarih;
        TextView islem;
        TextView cariBak;
        TextView cariBrc;
        TextView cariAlc;
        TextView cariDes;
    }
}