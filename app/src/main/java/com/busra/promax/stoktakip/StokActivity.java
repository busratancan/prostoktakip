package com.busra.promax.stoktakip;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.busra.promax.stoktakip.Models.Adapters.CariListAdapter;
import com.busra.promax.stoktakip.Models.Adapters.StokHareketAdapter;
import com.busra.promax.stoktakip.Models.CariDetayActivity;
import com.busra.promax.stoktakip.Models.CariModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class StokActivity  extends AppCompatActivity {
    ListView listView;
    StokHareketAdapter stokHareketAdapter;
    String url = "https://www.androidevreni.com/dersler/json_veri.txt";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stok);
        listView = (ListView) findViewById ( R.id.stok_list );
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        final ArrayList<CariModel> deneme = new ArrayList<>();
        final JsonArrayRequest myReq = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d("URL", "URL: " + url);
                Gson gson = new Gson();
                Type collectionType = new TypeToken<ArrayList<CariModel>>() {
                }.getType();
                ArrayList<CariModel> enums = gson.fromJson(response.toString(), collectionType);
                for (CariModel list : enums) {
                    Log.d("URL", "URL: " + list.getName());
                    deneme.add(list);
                }
                stokHareketAdapter = new StokHareketAdapter(getApplicationContext(), deneme);
                listView.setAdapter(stokHareketAdapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent i = new Intent(getApplicationContext(), CariDetayActivity.class);
                        i.putExtra("id","dene"+deneme.get(position).getName());
                        startActivity(i);
                    }
                });

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("TEST", "onErrorResponse: " + error);

            }
        });
        requestQueue.add(myReq);
    }
    }
