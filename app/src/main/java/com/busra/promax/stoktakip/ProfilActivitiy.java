package com.busra.promax.stoktakip;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ProfilActivitiy  extends Activity {

    Button set;
    Button kayit,login;
    EditText email,sifre;
    String email_string,sifre_string;
    SharedPreferences preferencess;
    SharedPreferences.Editor editors;
    SharedPreferences preferences; //preferences nesne referansı
    SharedPreferences.Editor editor; //preferences editor nesnesi referansı .prefernces nesnesine veri ekleyip cıkarmak için
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hesap);
        preferencess = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());//preferences objesi
        editors = preferencess.edit(); //aynı şekil editor nesnesi oluşturuluyor

        kayit = (Button) findViewById(R.id.sv_btn);
        login = (Button) findViewById(R.id.login_btn);
        email = (EditText)findViewById(R.id.mail_edt);
        sifre = (EditText)findViewById(R.id.password_edt);
        FloatingActionButton myFab = (FloatingActionButton)findViewById(R.id.fab);

        myFab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),SettingsActivity.class);
                startActivity(i);
                finish();
            }
        });

        login.setOnClickListener(new View.OnClickListener() {//giriş butonu tıklandığı zaman

            @Override
            public void onClick(View v) {
                email_string = email.getText().toString();
                sifre_string = sifre.getText().toString();

                if(email_string.matches("") || sifre_string.matches("")){//bilgiler eksik   ise

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(ProfilActivitiy.this);
                    alertDialog.setTitle("Uyarı");
                    alertDialog.setMessage("Eksiksiz Doldurunz!");
                    alertDialog.setPositiveButton("Tamam", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int which) {

                        }
                    });
                    alertDialog.show();
                }else{//Bilgiler doğru ise

                    String email = preferencess.getString("email", "");//preferences objesinden kaydettiğimiz değerleri key leri ile geri alıyoruz
                    String sifre = preferencess.getString("sifre", "");//preferences objesinden kaydettiğimiz değerleri key leri ile geri alıyoruz

                    if(email.matches(email_string) && sifre.matches(sifre_string)){//edittextlerden alınan şifre ve mail değerleri shared preferencesdan alınan değerlerle eşleşiyorsa

                        //Giriş başarılı ise sharedpreferences login değerini true yapıyoruz.
                        editors.putBoolean("login", true);
                        Intent i = new Intent(getApplicationContext(),MainActivity.class);//Anasayfa aktivity si açılır
                        startActivity(i);
                        finish();
                    }else{//şifre ve mail uyuşmuyorsa hata bastırılır
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ProfilActivitiy.this);
                        alertDialog.setTitle("Hata");
                        alertDialog.setMessage("Mailiniz veya Şifreniz Uyuşmuyor!");
                        alertDialog.setPositiveButton("Tamam", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int which) {

                            }
                        });
                        alertDialog.show();
                    }
                }
            }
        });

        kayit.setOnClickListener(new View.OnClickListener() {//Kayıt ol butonu tıklanınca

            @Override
            public void onClick(View v) {
                email_string = email.getText().toString();
                sifre_string = sifre.getText().toString();
                if(email_string.matches("") || sifre_string.matches("")){//Veriler Boş  ise

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(ProfilActivitiy.this);
                    alertDialog.setTitle("Uyarı");
                    alertDialog.setMessage("Eksiksiz Doldurunz!");
                    alertDialog.setPositiveButton("Tamam", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int which) {

                        }
                    });
                    alertDialog.show();
                }else{//veriler boş değilse kayıt işlemine geçilir

                    preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());//preferences nesnesi oluşturuluyor ve prefernces referansına bağlanıyor
                    editor = preferences.edit(); //aynı şekil editor nesnesi oluşturuluyor


                    editor.putString("email", email_string);//email değeri
                    editor.putString("sifre", sifre_string);//şifre değeri
                    editor.putBoolean("login", true);//uygulamaya tekrar girdiğinde kontrol için kullanılcak
                    //editor.putInt("sayısalDeger", 1000);// uygulamamızda kullanılmıyor ama göstermek amacıyla

                    editor.apply();;//yapılan değişiklikler kaydedilmesi için editor nesnesinin commit() metodu çağırılır.
                    //Değerlerimizi sharedPreferences a kaydettik.Artık bu bilgiler ile giriş yapabiliriz.
                    Intent i = new Intent(getApplicationContext(),MainActivity.class);
                    startActivity(i);
                    finish();
                }

            }
        });
    }
}