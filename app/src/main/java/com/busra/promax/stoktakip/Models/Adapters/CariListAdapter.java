package com.busra.promax.stoktakip.Models.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.busra.promax.stoktakip.Models.CariModel;
import com.busra.promax.stoktakip.R;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class CariListAdapter extends ArrayAdapter<CariModel> {

    private final LayoutInflater inflater;
    private final Context context;
    private CariListAdapter.ViewHolder holder;
    private final ArrayList<CariModel> cariModels;

    public CariListAdapter(Context context, ArrayList<CariModel> cariModels) {
        super(context,0, cariModels);
        this.context = context;
        this.cariModels = cariModels;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return cariModels.size();
    }

    @Override
    public CariModel getItem(int position) {
        return cariModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return cariModels.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.cari_list_item, null);

            holder = new CariListAdapter.ViewHolder();
            holder.cariId = (TextView) convertView.findViewById(R.id.cari_id_txt);
            holder.cariUnv = (TextView) convertView.findViewById(R.id.cari_unvani_txt);
            convertView.setTag(holder);

        }
        else{
            //Get viewholder we already created
            holder = (CariListAdapter.ViewHolder)convertView.getTag();
        }

        CariModel cariModel = cariModels.get(position);
        if(cariModel != null){
            holder.cariId.setText(cariModel.getName());
            holder.cariUnv.setText(cariModel.getCity());

        }


        return convertView;
    }

    //View Holder Pattern for better performance
    private static class ViewHolder {
        TextView cariId;
        TextView cariUnv;
    }
}